#include "stdafx.h"
#include "ImageScreenerApp.h"
#include "CTCViewerDlg.h"

ImageScreenerApp theApp;

BOOL ImageScreenerApp::InitInstance()
{
	CCTCViewerDlg dlg;
	m_pMainWnd = &dlg;
	dlg.DoModal();
	return FALSE;
}