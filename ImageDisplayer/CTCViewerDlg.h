
// CTCViewerDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include <vector>
#include "afxcmn.h"
#include "resource.h"
#include "RegionType.h"
#include "OverlayTIFFData.h"

using namespace std;

// CCTCViewerDlg dialog
class CCTCViewerDlg : public CDialogEx
{
// Construction
public:
	CCTCViewerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CTCVIEWER_DIALOG };

	protected:	
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	CStatic m_ImageDisplay;
	CImage m_Image;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedLoad();
	afx_msg void OnBnClickedExport();
	afx_msg void OnBnClickedBatch();
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	CString m_Filename;
	vector<REGIONTYPE> m_Regions;
	COverlayTIFFData *m_OverlayTIFFData;
	BOOL Save24BitBMP(CString PathName, CImage *image);
	CButton m_CTCOnly;
	CButton m_AllTypes;
	CString m_Status;
	BOOL ProcessOneSample(CString imagefile, CString regionfile);
	afx_msg LRESULT OnEnableButtons(WPARAM wparam, LPARAM lparam);
	bool LoadRegionData(CString regionfile);
};




