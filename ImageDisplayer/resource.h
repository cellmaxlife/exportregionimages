//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 產生的 Include 檔案。
// 由 CTCViewer.rc 使用
//
#define IDD_CTCVIEWER_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_IMAGEDISPLAY                1000
#define IDC_LOAD                        1001
#define IDC_SAVERGN                     1002
#define IDC_SCROLLVIEW                  1003
#define IDC_SLIDER4                     1011
#define IDC_MAGSLIDE                    1011
#define IDC_EXPORT                      1012
#define IDC_ALLTYPES                    1013
#define IDC_CTCONLY                     1014
#define IDC_BATCH                       1015
#define IDC_STATUS                      1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
