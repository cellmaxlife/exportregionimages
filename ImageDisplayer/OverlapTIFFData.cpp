#include "stdafx.h"
#include "OverlayTIFFData.h"

using namespace std;

#define SUBSAMPLERATE	32

const int TIFFTAG_IMAGEWIDTH = 256;	/* image width in pixels */
const int TIFFTAG_IMAGELENGTH = 257;	/* image height in pixels */
const int TIFFTAG_BITSPERSAMPLE = 258;	/* bits per channel (sample) */
const int TIFFTAG_COMPRESSION = 259;	/* data compression technique */
const int COMPRESSION_NONE = 1;	/* dump mode */
const int COMPRESSION_CCITTRLE = 2;	/* CCITT modified Huffman RLE */
const int COMPRESSION_CCITTFAX3 = 3;	/* CCITT Group 3 fax encoding */
const int COMPRESSION_CCITTFAX4 = 4;	/* CCITT Group 4 fax encoding */
const int COMPRESSION_LZW = 5;	/* Lempel-Ziv  & Welch */
const int COMPRESSION_JPEG = 6;	/* !JPEG compression */
const int TIFFTAG_PHOTOMETRIC = 262;	/* photometric interpretation */
const int PHOTOMETRIC_MINISWHITE = 0;	/* min value is white */
const int PHOTOMETRIC_MINISBLACK = 1;	/* min value is black */
const int PHOTOMETRIC_RGB = 2;	/* RGB color model */
const int PHOTOMETRIC_PALETTE = 3;	/* color map indexed */
const int PHOTOMETRIC_MASK = 4;	/* $holdout mask */
const int PHOTOMETRIC_SEPARATED = 5;	/* !color separations */
const int PHOTOMETRIC_YCBCR = 6;	/* !CCIR 601 */
const int PHOTOMETRIC_CIELAB = 8;	/* !1976 CIE L*a*b* */
const int TIFFTAG_STRIPOFFSETS = 273;	/* offsets to data strips */
const int TIFFTAG_SAMPLESPERPIXEL = 277;	/* samples per pixel */
const int TIFFTAG_ROWSPERSTRIP = 278;	/* rows per strip of data */
const int TIFFTAG_STRIPBYTECOUNTS = 279;	/* bytes counts for strips */

COverlayTIFFData::COverlayTIFFData()
{
	m_Width = 0;
	m_Height = 0;
	m_pBlueImage = NULL;
	m_pGreenImage = NULL;
	m_pRedImage = NULL;
}

COverlayTIFFData::~COverlayTIFFData(void)
{
	FreeOverlayTIFFData();
}

void COverlayTIFFData::FreeOverlayTIFFData(void)
{
	FreeRowImageData(m_pBlueImage);
	m_pBlueImage = NULL;
	FreeRowImageData(m_pGreenImage);
	m_pGreenImage = NULL;
	FreeRowImageData(m_pRedImage);
	m_pRedImage = NULL;
	
	m_Width = 0;
	m_Height = 0;
}

void COverlayTIFFData::FreeRowImageData(BYTE **image)
{
	if (image != NULL)
	{
		for (int i = 0; i < m_Height; i++)
		{
			delete[] image[i];
			image[i] = NULL;
		}

		delete[] image;
	}
}

int COverlayTIFFData::GetImageWidth(void)
{
	return m_Width;
}

int COverlayTIFFData::GetImageHeight(void)
{
	return m_Height;
}

BOOL COverlayTIFFData::LoadOverlayTIFFFile(CString filename)
{
	BOOL status = FALSE;
	
	FreeOverlayTIFFData();
	CFile theFile;
	if (theFile.Open(filename, CFile::modeRead))
	{
		BYTE *buf = NULL;
		UINT32 *StripOffsets = NULL;
		UINT32 *StripByteCounts = NULL;

		while (TRUE)
		{
			ULONGLONG numBytes = theFile.GetLength();
			if (numBytes < 8)
			{
				break;
			}
			buf = new BYTE[8];
			UINT count = theFile.Read(buf, 8);
			if (count != 8)
			{
				break;
			}
			if ((buf[0] != buf[1]) || (buf[0] != 'I'))
			{
				break;
			}
			UINT32 *ptr;
			ptr = (UINT32*)&buf[4];
			if (*ptr >= numBytes)
			{
				break;
			}
			theFile.Seek(*ptr, CFile::begin);
			int len = (int)(numBytes - *ptr);
			if (buf != NULL)
			{
				delete[] buf;
				buf = NULL;
			}
			buf = new BYTE[len];
			count = theFile.Read(buf, len);
			if (count != len)
			{
				break;
			}
			UINT16 numTags = *((UINT16*)&buf[0]);
			UINT32 offset = 2;
			UINT16 tag;
			UINT16 fieldType;
			UINT32 numOfValues;
			BOOL exitWhileLoop = FALSE;
			UINT32 CountForStripOffsetArray;
			UINT32 OffsetToStripOffsetArray;
			UINT32 CountForStripByteCountArray;
			UINT32 OffsetToStripByteCountArray;
			for (int i = 0; i < numTags; i++, offset += 12)
			{
				tag = *((UINT16 *)&buf[offset]);
				if (tag == TIFFTAG_IMAGEWIDTH)
				{
					m_Width = *((UINT32 *)&buf[offset + 8]);
				}
				else if (tag == TIFFTAG_IMAGELENGTH)
				{
					m_Height = *((UINT32 *)&buf[offset + 8]);
				}
				else if (tag == TIFFTAG_BITSPERSAMPLE)
				{
					fieldType = *((UINT16 *)&buf[offset + 2]);
					numOfValues = *((UINT32 *)&buf[offset + 4]);
					if (numOfValues == 1)
					{
						exitWhileLoop = TRUE;
						break;
					}
					else
					{
						BYTE *buf2 = new BYTE[6];
						UINT32 len2 = 2 * numOfValues;
						UINT32 offset2 = *((UINT32 *)&buf[offset + 8]);
						theFile.Seek(offset2, CFile::begin);
						int count2 = theFile.Read(buf2, len2);
						if (count2 != len2)
						{
							delete buf2;
							buf2 = NULL;
							break;
						}
						else
						{
							UINT16 value1 = *((UINT16 *)&buf2[0]);
							UINT16 value2 = *((UINT16 *)&buf2[2]);
							UINT16 value3 = *((UINT16 *)&buf2[4]);
							delete buf2;
							buf2 = NULL;
							if ((value1 != 8) || (value2 != 8) || (value3 != 8))
							{
								exitWhileLoop = TRUE;
								break;
							}
						}
					}
				}
				else if (tag == TIFFTAG_STRIPOFFSETS)
				{
					CountForStripOffsetArray = *((UINT32 *)&buf[offset + 4]);
					OffsetToStripOffsetArray = *((UINT32 *)&buf[offset + 8]);
				}
				else if (tag == TIFFTAG_STRIPBYTECOUNTS)
				{
					CountForStripByteCountArray = *((UINT32 *)&buf[offset + 4]);
					OffsetToStripByteCountArray = *((UINT32 *)&buf[offset + 8]);
				}
			}

			if (exitWhileLoop)
				break;

			StripOffsets = new UINT32[CountForStripOffsetArray];
				
			theFile.Seek(OffsetToStripOffsetArray, CFile::begin);
			len = (int)(4 * CountForStripOffsetArray);
			if (buf != NULL)
			{
				delete[] buf;
				buf = NULL;
			}
			buf = new BYTE[len];
			count = theFile.Read(buf, len);
			if (count != len)
			{
				break;
			}

			offset = 0;
			for (int j = 0; j < (int)CountForStripOffsetArray; j++, offset += 4)
			{
				StripOffsets[j] = *((UINT32 *)&buf[offset]);
			}
			StripByteCounts = new UINT32[CountForStripByteCountArray];
			theFile.Seek(OffsetToStripByteCountArray, CFile::begin);
			len = (int)(4 * CountForStripByteCountArray);
			if (buf != NULL)
			{
				delete[] buf;
				buf = NULL;
			}
			buf = new BYTE[len];
			count = theFile.Read(buf, len);
			if (count != len)
			{
				break;
			}
			
			offset = 0;
			for (int j = 0; j < (int)CountForStripByteCountArray; j++, offset += 4)
			{
				StripByteCounts[j] = *((UINT32 *)&buf[offset]);
			}
			m_pRedImage = new BYTE*[m_Height];
			m_pGreenImage = new BYTE*[m_Height];
			m_pBlueImage = new BYTE*[m_Height];
			for (int i = 0; i < m_Height; i++)
			{
				m_pRedImage[i] = new BYTE[m_Width];
				m_pGreenImage[i] = new BYTE[m_Width];
				m_pBlueImage[i] = new BYTE[m_Width];
			}
				
			int rowIndex = 0;
			int columnIndex = 0;
			int numPixels;

			for (int i = 0; i < (int)CountForStripOffsetArray; i++)
			{
				theFile.Seek(StripOffsets[i], CFile::begin);
				len = (int)StripByteCounts[i];
				if (buf != NULL)
				{
					delete[] buf;
					buf = NULL;
				}
				buf = new BYTE[len];
				count = theFile.Read(buf, len);
				if (count != len)
				{
					break;
				}

				offset = 0;
				numPixels = (int)(len / 3);
				for (int j = 0; j < numPixels; j++, offset += 3)
				{
					m_pRedImage[rowIndex][columnIndex] = buf[offset];
					m_pGreenImage[rowIndex][columnIndex] = buf[offset+1];
					m_pBlueImage[rowIndex][columnIndex] = buf[offset + 2];
					columnIndex++;
					if (columnIndex == m_Width)
					{
						columnIndex = 0;
						rowIndex++;
					}
				}
			}
			status = TRUE;
			break;
		}
			
		if (buf != NULL)
		{
			delete[] buf;
			buf = NULL;
		}
		if (StripOffsets != NULL)
		{
			delete[] StripOffsets;
			StripOffsets = NULL;
		}
		if (StripByteCounts != NULL)
		{
			delete[] StripByteCounts;
			StripByteCounts = NULL;
		}
			
		theFile.Close();
	}
	return status;
}

BOOL COverlayTIFFData::CopyROIImage(CImage *image, int x0, int y0, int width, int height)
{
	BOOL status = FALSE;
	if ((x0 >= 0) && (y0 >= 0) && ((x0 + width) <= m_Width)
		&& ((y0 + height) <= m_Height))
	{
		BYTE *ptr = (BYTE *) (image->GetBits());
		int nStride = image->GetPitch() - (width * 3);
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				*ptr++ = m_pBlueImage[y0 + i][x0 + j];
				*ptr++ = m_pGreenImage[y0 + i][x0 + j];
				*ptr++ = m_pRedImage[y0 + i][x0 + j];
			}

			if (nStride > 0)
			{
				ptr += nStride;
			}
		}
		status = TRUE;
	}
	return status;
}

int COverlayTIFFData::GetSubsampledWidth()
{
	return (m_Width / SUBSAMPLERATE);
}

int COverlayTIFFData::GetSubsampledHeight()
{
	return (m_Height / SUBSAMPLERATE);
}

bool COverlayTIFFData::GetSubsampleImage(CImage *image)
{
	bool ret = true;
	BYTE *ptr = (BYTE *) image->GetBits();
	int pitch = image->GetPitch();
	int width = GetSubsampledWidth();
	int height = GetSubsampledHeight();

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			ptr[i * pitch + 3 * j] = m_pBlueImage[i * SUBSAMPLERATE][j * SUBSAMPLERATE];
			ptr[i * pitch + 3 * j + 1] = m_pGreenImage[i * SUBSAMPLERATE][j * SUBSAMPLERATE];
			ptr[i * pitch + 3 * j + 2] = m_pRedImage[i * SUBSAMPLERATE][j * SUBSAMPLERATE];
		}
	}

	return ret;
}