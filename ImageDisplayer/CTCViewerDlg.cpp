﻿
// CTCViewerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTCViewerDlg.h"
#include "afxdialogex.h"
#include "string.h"
#include <atlstr.h>
#include "ColorType.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCTCViewerDlg dialog
static TCHAR szPathName[MAX_PATH];

#define WM_MY_MESSAGE (WM_USER+1001)
#define WM_ENABLE_BUTTONS (WM_USER+1002)

static DWORD WINAPI BatchRunOperation(LPVOID param);

CCTCViewerDlg::CCTCViewerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCTCViewerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCTCViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGEDISPLAY, m_ImageDisplay);
	DDX_Control(pDX, IDC_ALLTYPES, m_AllTypes);
	DDX_Control(pDX, IDC_CTCONLY, m_CTCOnly);
	DDX_Text(pDX, IDC_STATUS, m_Status);
}

BEGIN_MESSAGE_MAP(CCTCViewerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CCTCViewerDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_LOAD, &CCTCViewerDlg::OnBnClickedLoad)
	ON_BN_CLICKED(IDC_EXPORT, &CCTCViewerDlg::OnBnClickedExport)
	ON_BN_CLICKED(IDC_BATCH, &CCTCViewerDlg::OnBnClickedBatch)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_MESSAGE(WM_ENABLE_BUTTONS, OnEnableButtons)
END_MESSAGE_MAP()

// CCTCViewerDlg message handlers

BOOL CCTCViewerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_Filename = _T("");
	szPathName[0] = _T('');
	m_Status = _T("");
	CButton *btn = (CButton *)GetDlgItem(IDC_EXPORT);
	btn->EnableWindow(FALSE);
	m_OverlayTIFFData = new COverlayTIFFData();
	m_AllTypes.SetCheck(BST_CHECKED);
	return TRUE;  // return TRUE  unless you set the focus to a control
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCTCViewerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		if (m_Image != NULL)
		{
			CPaintDC dc(&m_ImageDisplay);
			CRect rect;
			m_ImageDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Image.StretchBlt(dc.m_hDC, rect);
		}
		if (m_Regions.size() > 0)
		{
			CDC* pDC = m_ImageDisplay.GetDC();
			CPen CursorPen(PS_SOLID, 1, RGB(255, 255, 0));
			CPen *pOldPen = pDC->SelectObject(&CursorPen);
			CRect rect;
			m_ImageDisplay.GetClientRect(&rect);
			pDC->SelectObject(pOldPen);
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextAlign(TA_LEFT);
			double xScale = ((double)m_OverlayTIFFData->GetImageWidth()) / ((double)rect.Width());
			double yScale = ((double)m_OverlayTIFFData->GetImageHeight()) / ((double)rect.Height());
			for (int i = 0; i < (int)m_Regions.size(); i++)
			{
				int x0 = (int)(((double)m_Regions[i].x) / xScale);
				int y0 = (int)(((double)m_Regions[i].y) / yScale);
				CRect rect1;
				rect1.left = rect.left + x0;
				rect1.top = rect.top + y0;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				CString label;
				label.Format(_T("%d"), (i + 1));
				if (m_Regions[i].color == CTC)
					pDC->SetTextColor(RGB(255, 0, 0));
				else if (m_Regions[i].color == CTC2)
					pDC->SetTextColor(RGB(255, 0, 255));
				else
					pDC->SetTextColor(RGB(0, 255, 255));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
			}
		}
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCTCViewerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

LRESULT CCTCViewerDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

void CCTCViewerDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	delete m_OverlayTIFFData;
	m_Regions.clear();
	CDialogEx::OnCancel();
}

bool CCTCViewerDlg::LoadRegionData(CString regionFilename)
{
	bool ret = false;
	const CString TAG1 = _T("0 1, 1 ");
	const CString TAG2 = _T(", 2 ");
	const CString TAG3 = _T(", ");

	CStdioFile theFile;
	if (theFile.Open(regionFilename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			int index = textline.Find(TAG1);
			if (index > -1)
			{
				textline = textline.Mid(TAG1.GetLength());
				index = textline.Find(TAG2);
				if (index > -1)
				{
					unsigned int color = _wtoi(textline.Mid(0, index));
					if ((color != CTC) && (color != CTC2))
						color = NONCTC;
					textline = textline.Mid(index + TAG2.GetLength());
					index = textline.Find(_T(" "));
					if (index > -1)
					{
						int x0 = _wtoi(textline.Mid(0, index));
						textline = textline.Mid(index + 1);
						index = textline.Find(TAG3);
						if (index > -1)
						{
							int y0 = _wtoi(textline.Mid(0, index));
							REGIONTYPE rgn;
							rgn.x = x0;
							rgn.y = y0;
							rgn.color = color;
							m_Regions.push_back(rgn);
							continue;
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
		theFile.Close();
	}
	if (m_Regions.size() > 0)
	{
		ret = true;
	}

	return ret;
}


void CCTCViewerDlg::OnBnClickedLoad()
{
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_EXPORT);
	btn->EnableWindow(FALSE);
	m_Regions.clear();
	
	bool ret = false;
	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Overlay files (*.tif)|*.tif"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_Filename = dlg.GetPathName();
		if (m_OverlayTIFFData->LoadOverlayTIFFFile(m_Filename))
		{
			ret = true;
			if (m_Image != NULL)
				m_Image.Destroy();
			m_Image.Create(m_OverlayTIFFData->GetSubsampledWidth(), -m_OverlayTIFFData->GetSubsampledHeight(), 24);
			m_OverlayTIFFData->GetSubsampleImage(&m_Image);
		}
		else
		{
			m_Status = _T("Fail to Load Overlay");
			UpdateData(FALSE);
		}
	}
	if (ret)
	{
		CFileDialog dlg1(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("Region files (*.rgn)|*.rgn"), NULL, 0, TRUE);
		if (dlg1.DoModal() == IDOK)
		{
			CString regionFilename = dlg1.GetPathName();
			ret = LoadRegionData(regionFilename);
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(TRUE);
	if (m_Regions.size() > 0)
	{
		btn = (CButton *)GetDlgItem(IDC_EXPORT);
		btn->EnableWindow(TRUE);
		m_Status = _T("Regions are Loaded");
		UpdateData(FALSE);
	}
	CRect rect;
	m_ImageDisplay.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}

void CCTCViewerDlg::OnBnClickedExport()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	BOOL ret = FALSE;
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_EXPORT);
	btn->EnableWindow(FALSE);

	BROWSEINFO   bi;
	ZeroMemory(&bi, sizeof(bi));

	bi.hwndOwner = NULL;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szPathName;
	bi.lpszTitle = _T("Please select a folder:");
	bi.ulFlags = BIF_RETURNONLYFSDIRS;
	bi.lParam = NULL;
	bi.iImage = 0;

	LPITEMIDLIST   pidl = SHBrowseForFolder(&bi);
	if (NULL != pidl)
	{
		ret = SHGetPathFromIDList(pidl, szPathName);
	}

	CString sampleName = m_Filename;
	if (ret)
	{
		CString overlayStr = _T("Overlay_");
		int index = sampleName.Find(overlayStr);
		if (index == -1)
		{
			ret = FALSE;
			m_Status = _T("Fail to Get SampleName");
			UpdateData(FALSE);
		}
		else {
			sampleName = sampleName.Mid(index + overlayStr.GetLength());
			index = sampleName.Find(_T("_"));
			if (index > 0)
			{
				sampleName = sampleName.Mid(0, index);
			}
			else
			{
				ret = FALSE;
				m_Status = _T("Fail to Get SampleName");
				UpdateData(FALSE);
			}
		}
	}
	CString newPath = _T("");
	if (ret)
	{
		newPath.Format(_T("%s\\%s"), szPathName, sampleName);
		if (!CreateDirectory(newPath, NULL)) {
			if (GetLastError() == ERROR_ALREADY_EXISTS) {
				// directory already exists
				m_Status = _T("Folder Already Exists");
				UpdateData(FALSE);
			}
			else {
				// creation failed due to some other reason
				m_Status = _T("Fail to Open a Folder");
				UpdateData(FALSE);
			}
			ret = FALSE;
		}
	}
	if (ret)
	{
		int count = 0;
		for (int i = 0; i < (int)m_Regions.size(); i++)
		{
			int x = m_Regions[i].x;
			int y = m_Regions[i].y;
			unsigned int color = m_Regions[i].color;
			if ((m_AllTypes.GetCheck() == BST_CHECKED) ||
				((m_CTCOnly.GetCheck() == BST_CHECKED) && ((color == CTC) || (color == CTC2))))
			{
				CString CellType = _T("CTC");
				if (color == NONCTC)
					CellType = _T("NONCTC");
				CImage *image;
				image = new CImage();
				image->Create(100, -100, 24);
				ret = m_OverlayTIFFData->CopyROIImage(image, x, y, 100, 100);
				if (ret)
				{
					CString pathname;
					pathname.Format(_T("%s\\Cell%d_%s.bmp"), newPath, i + 1, CellType);
					Save24BitBMP(pathname, image);
					count++;
				}
				delete image;
			}
		}
		m_Status.Format(_T("%d Files Saved"), count);
		UpdateData(FALSE);
	}
	btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_EXPORT);
	btn->EnableWindow(TRUE);
}

BOOL CCTCViewerDlg::Save24BitBMP(CString PathName, CImage *image)
{
	BOOL ret = FALSE;

	CFile fileToSave;
	ret = fileToSave.Open(PathName, CFile::modeCreate | CFile::modeWrite);
	if (ret)
	{
		BITMAPINFOHEADER BMIH;
		memset(&BMIH, 0, 40);
		BMIH.biSize = 40;
		BMIH.biSizeImage = image->GetPitch() * image->GetHeight();
		BMIH.biWidth = image->GetWidth();
		BMIH.biHeight = image->GetHeight();
		if (BMIH.biHeight > 0)
			BMIH.biHeight = -BMIH.biHeight;
		BMIH.biPlanes = 1;
		BMIH.biBitCount = 24;
		BMIH.biCompression = BI_RGB;

		BITMAPFILEHEADER bmfh;
		memset(&bmfh, 0, 14);
		int nBitsOffset = 14 + BMIH.biSize;
		LONG lImageSize = BMIH.biSizeImage;
		LONG lFileSize = nBitsOffset + lImageSize;

		bmfh.bfType = 'B' + ('M' << 8);
		bmfh.bfOffBits = nBitsOffset;
		bmfh.bfSize = lFileSize;
		bmfh.bfReserved1 = bmfh.bfReserved2 = 0;

		fileToSave.Write(&bmfh, 14);
		fileToSave.Write(&BMIH, 40);
		fileToSave.Write(image->GetBits(), lImageSize);
		fileToSave.Close();
	}

	return ret;
}

void CCTCViewerDlg::OnBnClickedBatch()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	BOOL ret = FALSE;
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_EXPORT);
	btn->EnableWindow(FALSE);

	BROWSEINFO   bi;
	ZeroMemory(&bi, sizeof(bi));

	bi.hwndOwner = NULL;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szPathName;
	bi.lpszTitle = _T("Please select a folder:");
	bi.ulFlags = BIF_RETURNONLYFSDIRS;
	bi.lParam = NULL;
	bi.iImage = 0;

	LPITEMIDLIST   pidl = SHBrowseForFolder(&bi);
	if (NULL != pidl)
	{
		ret = SHGetPathFromIDList(pidl, szPathName);
	}
	else
	{
		m_Status = _T("Fail to Select Folder");
		UpdateData(FALSE);
	}

	if (ret)
	{
		ret = FALSE;
		CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("Batch Scripts (*.txt)|*.txt"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			m_Filename = dlg.GetPathName();
			HANDLE thread = ::CreateThread(NULL, 0, BatchRunOperation, this, CREATE_SUSPENDED, NULL);

			if (!thread)
			{
				m_Status = _T("Fail to Launch Thread");
				UpdateData(FALSE);
			}
			else
			{
				ret = TRUE;
				::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
				::ResumeThread(thread);
				m_Status = _T("Batch Thread Launched");
				UpdateData(FALSE);
			}
		}
		else
		{
			m_Status = _T("Batch Script Not Selected");
			UpdateData(FALSE);
		}
	}

	if (!ret)
	{
		btn = (CButton *)GetDlgItem(IDC_LOAD);
		btn->EnableWindow(TRUE);
		btn = (CButton *)GetDlgItem(IDC_BATCH);
		btn->EnableWindow(TRUE);
		btn = (CButton *)GetDlgItem(IDC_EXPORT);
		btn->EnableWindow(TRUE);
	}
}

DWORD WINAPI BatchRunOperation(LPVOID param)
{
	CStdioFile theFile;
	CCTCViewerDlg *ptr = (CCTCViewerDlg *)param;
	CString message;
	BOOL success = TRUE;

	if (theFile.Open(ptr->m_Filename, CFile::modeRead | CFile::typeText))
	{
		int lineIdx = 0;
		CString textline;
		while (theFile.ReadString(textline))
		{
			ptr->m_Regions.clear();
			lineIdx++;
			int index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("Batch Script Line#%d Error: Fail to read image filename"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				success = false;
				break;
			}
			CString imagename = textline.Mid(0, index);
			textline = textline.Mid(index + 1);
			index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("Batch Script Line#%d Error: Fail to read region filename"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				success = false;
				break;
			}
			CString regionname = textline.Mid(0, index);
			success = ptr->ProcessOneSample(imagename, regionname);
			if (!success)
			{
				ptr->m_Status.Format(_T("Fail Sample #%d"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				break;
			}
			else
			{
				ptr->m_Status.Format(_T("Done Sample #%d"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}
		theFile.Close();
		if (success)
		{
			ptr->m_Status.Format(_T("Batch Run Completed"), lineIdx);
			::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}

	::PostMessage(ptr->GetSafeHwnd(), WM_ENABLE_BUTTONS, NULL, NULL);
	return 0;
}

BOOL CCTCViewerDlg::ProcessOneSample(CString imagefile, CString regionfile)
{
	BOOL ret = FALSE;

	CString sampleName = imagefile;
	CString overlayStr = _T("Overlay_");
	int index = sampleName.Find(overlayStr);
	if (index > -1)
	{
		sampleName = sampleName.Mid(index + overlayStr.GetLength());
		index = sampleName.Find(_T("_"));
		if (index > 0)
		{
			ret = TRUE;
			sampleName = sampleName.Mid(0, index);
		}
	}
	CString newPath = _T("");
	if (ret)
	{
		newPath.Format(_T("%s\\%s"), szPathName, sampleName);
		if (!CreateDirectory(newPath, NULL)) {
			ret = FALSE;
		}
	}
	if (ret)
	{
		ret = m_OverlayTIFFData->LoadOverlayTIFFFile(imagefile);
	}
	if (ret)
	{
		ret = LoadRegionData(regionfile);
	}
	if (ret)
	{
		int count = 0;
		for (int i = 0; i < (int)m_Regions.size(); i++)
		{
			int x = m_Regions[i].x;
			int y = m_Regions[i].y;
			unsigned int color = m_Regions[i].color;
			if ((m_AllTypes.GetCheck() == BST_CHECKED) ||
				((m_CTCOnly.GetCheck() == BST_CHECKED) && ((color == CTC) || (color == CTC2))))
			{
				CString CellType = _T("CTC");
				if (color == NONCTC)
					CellType = _T("NONCTC");
				CImage *image;
				image = new CImage();
				image->Create(100, -100, 24);
				ret = m_OverlayTIFFData->CopyROIImage(image, x, y, 100, 100);
				if (ret)
				{
					CString pathname;
					pathname.Format(_T("%s\\Cell%d_%s.bmp"), newPath, i + 1, CellType);
					Save24BitBMP(pathname, image);
					count++;
				}
				delete image;
				if (!ret)
					break;
			}
		}
	}

	return ret;
}

LRESULT CCTCViewerDlg::OnEnableButtons(WPARAM wparam, LPARAM lparam)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_EXPORT);
	btn->EnableWindow(TRUE);
	return 0;
}