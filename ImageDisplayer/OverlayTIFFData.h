#pragma once

#include "afxwin.h"

class COverlayTIFFData
{
public:
	COverlayTIFFData();
	virtual ~COverlayTIFFData();

protected:
	int m_Width;
	int m_Height;
	BYTE **m_pBlueImage;
	BYTE **m_pGreenImage;
	BYTE **m_pRedImage;

	void FreeRowImageData(BYTE **image);
	void FreeOverlayTIFFData(void);

public:
	BOOL LoadOverlayTIFFFile(CString filename);
	int GetImageWidth(void);
	int GetImageHeight(void);
	BOOL CopyROIImage(CImage *rgb, int x0, int y0, int width, int height);
	int GetSubsampledWidth();
	int GetSubsampledHeight();
	bool GetSubsampleImage(CImage *image);
};